#!/bin/bash

openssl rand -out KEY -base64 32
openssl rand -out .docker/mysql/root_password -base64 20
openssl rand -out .docker/mysql/admin_password -base64 20

DB_FILE=.docker/mysql/init_db.sql
DB_ENC=.docker/mysql/init_db
DB_DEC=init_db.sql

KEY=$(cat < KEY)
ROOT_PASSWORD=$(cat < .docker/mysql/root_password)
ADMIN_PASSWORD=$(cat < .docker/mysql/admin_password)

./generate-db.sh $DB_FILE $ROOT_PASSWORD $ADMIN_PASSWORD
./generate-entrypoint.sh "init_db" "init_db.sql" $KEY

openssl enc -aes-256-cbc -a -salt -in $DB_FILE -out $DB_ENC -pass pass:$KEY -md sha1