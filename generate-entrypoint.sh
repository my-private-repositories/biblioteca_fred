#!/bin/bash

DB_ENC=$1
DB_DEC=$2
KEY=$3

INIT_FOLDER=".docker/mysql/.init"
FILE_NAME=entrypoint.sh
FILE_PATH="${INIT_FOLDER}/entrypoint.sh"
IN_FILE="/mysql-script/${DB_ENC}"
OUT_FILE="/mysql-script/${DB_DEC}"

mkdir -p $INIT_FOLDER

cat <<EOF > $FILE_PATH
#!/bin/bash

openssl enc -d -aes-256-cbc -salt -a -in ${IN_FILE} -out ${OUT_FILE} -pass pass:${KEY} -md sha1

mysql -u root --password=root <${OUT_FILE}

rm -rf /mysql-script
EOF

chmod +x $FILE_PATH