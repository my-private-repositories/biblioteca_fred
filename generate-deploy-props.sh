#!/bin/bash

ADMIN_PASSWORD=$1
KEY=$2
OUT_PROPS_FILE=src/main/resources/props-deploy

cat <<EOL > props-deploy
spring.datasource.url"="jdbc:mysql://db/library?createDatabaseIfNotExist=true&serverTimezone=UTC;spring.datasource.username"="admin;spring.datasource.password"="${ADMIN_PASSWORD}
EOL

openssl enc -aes-256-cbc -salt -a -in props-deploy -out $OUT_PROPS_FILE -pass pass:$KEY -md sha1