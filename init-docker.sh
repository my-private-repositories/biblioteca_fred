#!/bin/bash

./mysql-params.sh

ROOT_PASSWORD=$(cat < .docker/mysql/root_password)
ADMIN_PASSWORD=$(cat < .docker/mysql/admin_password)
KEY=$(cat < KEY)

./generate-props.sh $ADMIN_PASSWORD $KEY
./generate-deploy-props.sh $ADMIN_PASSWORD $KEY
./buildPackage.sh
./generate-yaml.sh

echo "Docker environment ready!"