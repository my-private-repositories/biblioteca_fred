#!/bin/bash

#
# Valid parameter val or abort script
#
function valid()
{
  if [ $1 -ne 0 ]; then
    echo "$2"
    echo ""
    exit
  fi
}
# --------------------------------

mvn clean package -Dmaven.test.skip=true > /dev/null 2>&1

valid $? "Fail to build package!"