package com.bedeapps.firstspringproject.domain.model;

public enum UserRole {
    ADMIN,
    USER
}
