package com.bedeapps.firstspringproject.domain.exception;

import java.util.List;

public class AuthorsNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;
    
    private List<Long> m_ids;

    public AuthorsNotFoundException(List<Long> ids) {
        super("Autores não encontrados.");

        m_ids = ids;
    }

    public List<Long> getAuthorsIds() {
        return m_ids;
    }
}
