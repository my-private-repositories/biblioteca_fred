package com.bedeapps.firstspringproject.domain.exception;

public class BookNotFoundException extends NegocioException {

    private static final long serialVersionUID = 1L;

    public BookNotFoundException() {
        super("Livro não encontrado");
    }
}
