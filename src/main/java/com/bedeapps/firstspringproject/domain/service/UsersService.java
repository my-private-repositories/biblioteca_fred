package com.bedeapps.firstspringproject.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import com.bedeapps.firstspringproject.core.Constants;
import com.bedeapps.firstspringproject.domain.model.User;
import com.bedeapps.firstspringproject.domain.model.UserCredential;
import com.bedeapps.firstspringproject.domain.model.UserRole;
import com.bedeapps.firstspringproject.domain.repository.UsersRepository;

@Service
public class UsersService {

    @Autowired
    private UsersRepository m_repo;

    public User inserir(User user) {
        String pass = user.getCredential().getPassPhrase();

        user.getCredential().setUser(user);
        user.getCredential().setPassPhrase(Constants.passwordEncoder().encode(pass));

        return m_repo.save(user);
    }

    private User getAdminUser() {
        User u = new User();
        UserCredential creds = new UserCredential();

        u.setFirstname("admin");
        u.setLastName("admin");
        u.setAddress("address");
        u.setEmail("admin@email.com");

        creds.setUserName("admin");
        creds.setPassPhrase("admin");
        creds.setUserRole(UserRole.ADMIN);

        creds.setUser(u);

        u.setCredential(creds);

        return u;
    }

    @PostConstruct
    private void insertAdmin() {
        User u = m_repo.findUserByUserName("admin");

        if (u == null)
            inserir(getAdminUser());
    }
}
