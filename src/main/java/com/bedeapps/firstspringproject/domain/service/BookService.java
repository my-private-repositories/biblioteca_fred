package com.bedeapps.firstspringproject.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bedeapps.firstspringproject.domain.model.Book;
import com.bedeapps.firstspringproject.domain.repository.BooksRepository;

@Service
public class BookService {
    
    @Autowired
    private BooksRepository m_repository;
    
    public Book save(Book book) {
        return m_repository.save(book);
    }
}
