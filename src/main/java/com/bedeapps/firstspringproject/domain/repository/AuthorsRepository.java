package com.bedeapps.firstspringproject.domain.repository;

import java.util.List;
import java.util.Set;

import com.bedeapps.firstspringproject.domain.model.Author;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorsRepository extends JpaRepository<Author, Long>{
    
    @Query(value = "SELECT id FROM authors;", nativeQuery = true)
    public Set<Long> getIds();

    public List<Author> findAllByOrderByFirstNameAsc();
}
