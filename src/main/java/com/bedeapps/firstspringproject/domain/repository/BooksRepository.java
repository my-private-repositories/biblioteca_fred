package com.bedeapps.firstspringproject.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bedeapps.firstspringproject.domain.model.Book;

@Repository
public interface BooksRepository extends JpaRepository<Book, Long> {
    
    public List<Book> findByTitle(String title);
}
