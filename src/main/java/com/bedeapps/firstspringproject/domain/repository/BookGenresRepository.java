package com.bedeapps.firstspringproject.domain.repository;

import java.util.List;
import java.util.Set;

import com.bedeapps.firstspringproject.domain.model.BookGenre;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BookGenresRepository extends JpaRepository<BookGenre, Long>{
    
    @Query(value = "SELECT id FROM genres;", nativeQuery = true)
    public Set<Long> getIds();

    public List<BookGenre> findAllByOrderByNameAsc();
}
