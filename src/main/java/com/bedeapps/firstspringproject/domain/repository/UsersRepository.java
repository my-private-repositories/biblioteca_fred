package com.bedeapps.firstspringproject.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bedeapps.firstspringproject.domain.model.User;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {

    public boolean existsByEmail(String email);

    @Query(value = "SELECT * FROM users u INNER JOIN users_credential uc ON u.id = uc.user_id" + 
    " WHERE uc.user_name = :user_name", nativeQuery = true)
    public User findUserByUserName(@Param("user_name") String userName);

    @Query(value = "SELECT COUNT(*) FROM users_credential"
            + " WHERE LOWER(user_name) = :user_name", nativeQuery = true)
    public int existsByUserName(@Param("user_name") String userName);
}
