package com.bedeapps.firstspringproject.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class AppPropsLoader {

    private static Properties getDefaultProperties(String profile) {
        Properties props = new Properties();

        props.put("server.port", 8081);
        props.put("spring.thymeleaf.cache", false);
        props.put("spring.thymeleaf.enabled", true);
        props.put("spring.thymeleaf.prefix", "classpath:/templates/");
        props.put("spring.thymeleaf.suffix", ".html");
        props.put("spring.main.allow-bean-definition-overriding", true);
        props.put("spring.application.name", "Biblioteca do Fred");

        if (profile == null) {
            props.put("spring.jpa.show-sql", true);
            props.put("spring.jpa.properties.hibernate.format_sql", true);
            props.put("spring.jpa.properties.hibernate.show_sql", true);
            props.put("spring.jpa.properties.hibernate.use_sql_comments", true);
            props.put("logging.level.org.hibernate.type", "trace");
        }

        return props;
    }

    public static Properties getProperties(String profile) {

        try {
            String secret = System.getenv("SECRET");
            String propsFile = "/props";

            if (profile != null)
                propsFile += "-" + profile;

            InputStream inputStream = AppPropsLoader.class.getResourceAsStream(propsFile);
            String data = readFromInputStream(inputStream);

            String decryptedString = Aes.decrypt(secret, data).replace(System.getProperty("line.separator"), "");

            String[] listProps = decryptedString.split(";");

            Properties newProperties = getDefaultProperties(profile);

            for (String prop : listProps) {
                String[] newProp = prop.split("\"=\"");
                newProperties.put(newProp[0], newProp[1]);
            }

            return newProperties;
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
        }
        
        return null;
    }

    private static String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line);
            }
        }

        return resultStringBuilder.toString();
    }
}
