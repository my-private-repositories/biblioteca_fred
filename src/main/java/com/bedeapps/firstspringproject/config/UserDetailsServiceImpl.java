package com.bedeapps.firstspringproject.config;

import java.util.HashSet;
import java.util.Set;

import com.bedeapps.firstspringproject.domain.model.User;
import com.bedeapps.firstspringproject.domain.repository.UsersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    
    @Autowired
    private UsersRepository m_repo;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {
        User u = m_repo.findUserByUserName(username);

        Set <GrantedAuthority> auths = new HashSet<>();
        auths.add(new SimpleGrantedAuthority(u.getCredential().getUserRole().toString()));

        return new org.springframework.security.core.userdetails.User(u.getCredential().getUserName(),
        u.getCredential().getPassPhrase(), auths);
    }
}
