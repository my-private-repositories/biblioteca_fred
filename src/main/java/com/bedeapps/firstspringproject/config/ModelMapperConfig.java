package com.bedeapps.firstspringproject.config;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bedeapps.firstspringproject.api.model.UserModel;
import com.bedeapps.firstspringproject.domain.model.User;

@Configuration
public class ModelMapperConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();

        PropertyMap<UserModel, User> propertyMap = new PropertyMap<UserModel, User> (){
            protected void configure() {
                User u = new User();
                map().getCredential().setUser(u);
            }
        };
                
        mapper.addMappings(propertyMap);
        
        return mapper;
    }
}
