package com.bedeapps.firstspringproject;

import com.bedeapps.firstspringproject.config.AppPropsLoader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstSpringProjectApplication { 

    public static void main(String[] args) {
        
        SpringApplication app = new SpringApplication(FirstSpringProjectApplication.class);        
        
        app.setDefaultProperties(AppPropsLoader.getProperties(System.getenv("SPRING_PROFILES_ACTIVE")));

        app.run(args);
    }
}
