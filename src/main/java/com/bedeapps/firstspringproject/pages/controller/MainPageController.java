package com.bedeapps.firstspringproject.pages.controller;

import java.util.Locale;

import com.bedeapps.firstspringproject.core.Constants;
import com.bedeapps.firstspringproject.domain.repository.AuthorsRepository;
import com.bedeapps.firstspringproject.domain.repository.BookGenresRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainPageController {

    @Value("${spring.application.name}")
    private String m_appName;

    private final MessageSource m_msgSrc;

    @Autowired
    private AuthorsRepository m_authorRepo;

    @Autowired
    private BookGenresRepository m_genreRepo;

    @Autowired
    public MainPageController() {
        m_msgSrc = Constants.messageSource();
    }
    
    @RequestMapping(value = {"/", "index"})
    public ModelAndView homePage(Model model) {
        model.addAttribute("appName", m_appName);
        
        return new ModelAndView("home");
    }
    
    @RequestMapping("/registro")
    public String registroPage(Model model) {
        model.addAttribute("appName", m_appName);
        
        return "register";
    }

    @RequestMapping("/addbook")
    public String addBookPage(Model model) {
        model.addAttribute("authors", m_authorRepo.findAllByOrderByFirstNameAsc());
        model.addAttribute("genres", m_genreRepo.findAllByOrderByNameAsc());

        return "addbook";
    }

    @GetMapping("/login")
    public String login(Model model, String error, Locale locale) {
        if(error != null) {
            model.addAttribute("errorMessage", m_msgSrc.getMessage("login.error", null, locale));
        }

        if (isAuthenticated()) {
            return "redirect:index";
        }

        return "login";
    }

    @GetMapping("/403")
    public String forbidden() {
        return "403";
    }

    private boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || AnonymousAuthenticationToken.class.isAssignableFrom(authentication.getClass())) {
            return false;
        }

        return authentication.isAuthenticated();
    }
} 
