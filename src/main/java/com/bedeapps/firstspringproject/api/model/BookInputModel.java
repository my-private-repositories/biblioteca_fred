package com.bedeapps.firstspringproject.api.model;

import java.math.BigDecimal;
import java.util.Set;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bedeapps.firstspringproject.api.validators.exists.ExistsCheckService;
import com.bedeapps.firstspringproject.api.validators.exists.ExistsConstraint;

public class BookInputModel {

    private Long m_id;

    @NotBlank(message = "book.title.empty")
    @Size(max = 100, message = "Size.book.title")
    private String m_title;
    
    @NotBlank(message = "book.description.empty")
    @Size(max = 256, message = "Size.book.description")
    private String m_description;

    @NotNull(message = "book.price.empty")
    @DecimalMin(value = "0.01", inclusive = true, message = "book.price.min")
    @Digits(integer = 4, fraction = 2, message = "book.price.digits")
    private BigDecimal m_price;

    private byte[] m_image;

    @NotEmpty(message = "book.authors.empty")
    @ExistsConstraint(service = ExistsCheckService.class, fieldName = "id", message = "book.author.not.exist", serviceQualifier = "ExistsCheckServiceAuthor")  
    private Set<AuthorInputModel> m_authors;

    @NotEmpty(message = "book.genre.empty")
    @ExistsConstraint(service = ExistsCheckService.class, fieldName = "id", message = "book.genre.not.exist", serviceQualifier = "ExistsCheckServiceGenre")  
    private Set<BookGenreInputModel> m_genres;

    public Long getId() {
        return m_id;
    }

    public void setId(Long id) {
        this.m_id = id;
    }
    
    public String getTitle() {
        return m_title;
    }

    public void setTitle(String title) {
        this.m_title = title;
    }

    public String getDescription() {
        return m_description;
    }

    public void setDescription(String description) {
        this.m_description = description;
    }

    public BigDecimal getPrice() {
        return m_price;
    }

    public void setPrice(BigDecimal price) {
        this.m_price = price;
    }

    public byte[] getImage() {
        return m_image;
    }

    public void setImage(byte[] image) {
        this.m_image = image;
    }

    public Set<AuthorInputModel> getAuthors() {
        return m_authors;
    }

    public void setAuthors(Set<AuthorInputModel> authors) {
        this.m_authors = authors;
    }

    public Set<BookGenreInputModel> getGenres() {
        return m_genres;
    }

    public void setGenres(Set<BookGenreInputModel> genres) {
        this.m_genres = genres;
    }

    public static String mapFieldToEntity(String modelField) {
        if(modelField.equals("m_title"))
            return "title";

        if(modelField.equals("m_author"))
            return "author";
        
        return modelField;
    }
}
