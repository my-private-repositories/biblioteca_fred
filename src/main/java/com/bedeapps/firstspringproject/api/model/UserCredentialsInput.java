package com.bedeapps.firstspringproject.api.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class UserCredentialsInput {
    
    @NotBlank
    private String userName;
    
    @NotBlank
    @Email
    private String email;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
