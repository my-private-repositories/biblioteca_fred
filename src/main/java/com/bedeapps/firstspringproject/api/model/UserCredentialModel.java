package com.bedeapps.firstspringproject.api.model;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bedeapps.firstspringproject.api.validators.unique.*;
import com.bedeapps.firstspringproject.domain.model.UserRole;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserCredentialModel {
    
    @JsonIgnore
    private Long userId;
    
    @Size(max = 60, message = "Size.userCredential.userName")
    @NotBlank(message = "userName.NotBlank")
    @Unique(service = UniqueCheckService.class, fieldName = "userName", message = "userName.unique.violation")    
    private String userName;

    @JsonIgnore
    @NotBlank(message = "passPhrase.NotBlank")
    private String passPhrase;
    
    @NotNull(message = "userRole.NotNull")
    @Enumerated(EnumType.STRING)	
    private UserRole userRole;
    
    
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long id) {
        this.userId = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonIgnore
    public String getPassPhrase() {
        return passPhrase;
    }

    @JsonProperty
    public void setPassPhrase(String passPhrase) {
        this.passPhrase = passPhrase;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
}
