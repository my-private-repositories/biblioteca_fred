package com.bedeapps.firstspringproject.api.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class BookGenreModel {
    private Long m_id;

    @NotBlank(message = "genre.name.empty")
    @Size(max = 60, message = "Size.bookGenre.name")
    private String m_name;

    @NotBlank(message = "genre.description.empty")
    @Size(max = 256, message = "Size.bookGenre.description")
    private String m_description;

    public Long getId() {
        return m_id;
    }

    public void setId(Long id) {
        this.m_id = id;
    }

    public String getName() {
        return m_name;
    }

    public void setName(String name) {
        this.m_name = name;
    }

    public String getDescription() {
        return m_description;
    }

    public void setDescription(String description) {
        this.m_description = description;
    }    
}
