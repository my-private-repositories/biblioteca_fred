package com.bedeapps.firstspringproject.api.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class AuthorModel {
    private Long m_id;
    
    @NotBlank(message = "author.firstName.empty")
    @Size(max = 60, message = "Size.author.firstName")
    private String m_firstName;

    @NotBlank(message = "author.lastName.empty")
    @Size(max = 60, message = "Size.author.lastName")
    private String m_lastName;

    @NotBlank(message = "author.description.empty")
    @Size(max = 256, message = "Size.author.description")
    private String m_description;

    public Long getId() {
        return m_id;
    }

    public void setId(Long id) {
        this.m_id = id;
    }

    public String getFirstName() {
        return m_firstName;
    }

    public void setFirstName(String firstName) {
        this.m_firstName = firstName;
    }

    public String getLastName() {
        return m_lastName;
    }

    public void setLastName(String lastName) {
        this.m_lastName = lastName;
    }

    public String getDescription() {
        return m_description;
    }

    public void setDescription(String description) {
        this.m_description = description;
    }
}
