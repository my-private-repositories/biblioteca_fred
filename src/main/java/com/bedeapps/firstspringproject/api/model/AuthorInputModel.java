package com.bedeapps.firstspringproject.api.model;

import javax.validation.constraints.NotNull;

import com.bedeapps.firstspringproject.api.validators.Identifiable;

public class AuthorInputModel implements Identifiable {
    
    @NotNull(message = "book.author.id.null")
    private Long m_id;

    @Override
    public Long getId() {
        return m_id;
    }

    public void setId(Long id) {
        this.m_id = id;
    }
}
