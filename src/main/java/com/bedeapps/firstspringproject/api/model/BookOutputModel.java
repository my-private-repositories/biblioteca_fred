package com.bedeapps.firstspringproject.api.model;

import java.math.BigDecimal;
import java.util.Set;

public class BookOutputModel {
    
    private Long m_id;
    private String m_title;
    private String m_description;
    private BigDecimal m_price;
    private byte[] m_image;
    private Set<AuthorOutputModel> m_authors;
    private Set<BookGenreOutputModel> m_genres;

    public Long getId() {
        return m_id;
    }

    public void setId(Long id) {
        m_id = id;
    }

    public String getTitle() {
        return m_title;
    }

    public void setTitle(String title) {
        m_title = title;
    }

    public String getDescription() {
        return m_description;
    }

    public void setDescription(String description) {
        m_description = description;
    }

    public BigDecimal getPrice() {
        return m_price;
    }

    public void setPrice(BigDecimal price) {
        m_price = price;
    }

    public byte[] getImage() {
        return m_image;
    }

    public void setImage(byte[] image) {
        m_image = image;
    }

    public Set<AuthorOutputModel> getAuthors() {
        return m_authors;
    }

    public void setAuthors(Set<AuthorOutputModel> authors) {
        m_authors = authors;
    }

    public Set<BookGenreOutputModel> getGenres() {
        return m_genres;
    }

    public void setGenres(Set<BookGenreOutputModel> genres) {
        m_genres = genres;
    }
}
