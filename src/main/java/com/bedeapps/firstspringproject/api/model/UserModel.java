package com.bedeapps.firstspringproject.api.model;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bedeapps.firstspringproject.api.validators.unique.*;

public class UserModel {
    private Long id;
    
    @NotBlank(message = "firstName.NotBlank")
    @Size(max = 60, message = "Size.user.firstName")
    private String firstName;
    
    @NotBlank(message = "lastName.NotBlank")
    @Size(max = 60, message = "Size.user.lastName")
    private String lastName;

    @NotBlank(message = "address.NotBlank")
    @Size(max = 256, message = "Size.user.address")
    private String address;
    
    @NotBlank(message = "email.NotBlank")
    @Email(message = "email.IncorrectFormat")
    @Unique(service = UniqueCheckService.class, fieldName = "email", message = "email.unique.violation")    
    private String email;
    
    @NotNull
    @Valid
    private UserCredentialModel credential;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserCredentialModel getCredential() {
        return credential;
    }

    public void setCredential(UserCredentialModel credential) {
        this.credential = credential;
    }		
}
