package com.bedeapps.firstspringproject.api.validators;

public interface Identifiable {
    public Long getId();
}
