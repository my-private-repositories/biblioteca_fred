package com.bedeapps.firstspringproject.api.validators.exists;

import java.util.Set;

import com.bedeapps.firstspringproject.api.validators.Identifiable;
import com.bedeapps.firstspringproject.domain.repository.AuthorsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("ExistsCheckServiceAuthor")
public class ExistsCheckServiceAuthor implements ExistsCheckService {

    @Autowired
    private AuthorsRepository m_repo;

    @Override
    public boolean fieldValueExists(Object value, String fieldName) throws UnsupportedOperationException {
        if(!fieldName.equals("id"))
            throw new UnsupportedOperationException("Field name not supported");
            
        Set<?> objs = (Set<?>) value;
        Set<Long> ids = m_repo.getIds();

        for(Object obj : objs) {
            if(!(obj instanceof Identifiable))
                return false;

            if(!ids.contains(((Identifiable)obj).getId()))
                return false;
        }
        
        return true;
    }
}
