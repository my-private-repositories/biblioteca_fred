package com.bedeapps.firstspringproject.api.validators.exists;

import com.bedeapps.firstspringproject.api.validators.FieldValueExists;

public interface ExistsCheckService extends FieldValueExists { }
