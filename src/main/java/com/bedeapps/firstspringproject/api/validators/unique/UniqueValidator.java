package com.bedeapps.firstspringproject.api.validators.unique;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.bedeapps.firstspringproject.api.validators.ApplicationContextProvider;
import com.bedeapps.firstspringproject.api.validators.FieldValueExists;

public class UniqueValidator implements ConstraintValidator<Unique, Object> {

    private FieldValueExists service;
    private String fieldName;

    @Override
    public void initialize(Unique unique) {
        Class<? extends FieldValueExists> clazz = unique.service();
        this.fieldName = unique.fieldName();
        String serviceQualifier = unique.serviceQualifier();

        if (!serviceQualifier.equals("")) {
            this.service = (FieldValueExists) ApplicationContextProvider.getBean(serviceQualifier, clazz);
        } else {
            this.service = (FieldValueExists) ApplicationContextProvider.getBean(clazz);
        }
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        return !this.service.fieldValueExists(o, this.fieldName);
    }
}