package com.bedeapps.firstspringproject.api.validators.exists;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.bedeapps.firstspringproject.api.validators.ApplicationContextProvider;
import com.bedeapps.firstspringproject.api.validators.FieldValueExists;

public class ExistsConstraintValidator implements ConstraintValidator<ExistsConstraint, Object> {

    private FieldValueExists m_service;
    private String m_fieldName;

    @Override
    public void initialize(ExistsConstraint constraint) {
        Class<? extends FieldValueExists> clazz = constraint.service();
        m_fieldName = constraint.fieldName();
        String serviceQualifier = constraint.serviceQualifier();

        if (!serviceQualifier.equals("")) {
            m_service = (FieldValueExists) ApplicationContextProvider.getBean(serviceQualifier, clazz);
        } else {
            m_service = (FieldValueExists) ApplicationContextProvider.getBean(clazz);
        }
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if(value == null)
            return true;

        return m_service.fieldValueExists(value, m_fieldName);
    }
}
