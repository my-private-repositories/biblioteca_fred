package com.bedeapps.firstspringproject.api.validators.unique;

import com.bedeapps.firstspringproject.api.validators.FieldValueExists;

public interface UniqueCheckService extends FieldValueExists {
}
