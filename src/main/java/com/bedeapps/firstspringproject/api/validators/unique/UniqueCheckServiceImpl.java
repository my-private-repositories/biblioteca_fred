package com.bedeapps.firstspringproject.api.validators.unique;

import com.bedeapps.firstspringproject.domain.repository.UsersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UniqueCheckServiceImpl implements UniqueCheckService {

    @Autowired
    private UsersRepository m_repo;

    @Override
    public boolean fieldValueExists(Object value, String fieldName) throws UnsupportedOperationException {
        if(value == null)
            return false;

        if(fieldName.equals("email")) {
            return m_repo.existsByEmail(value.toString());
        }

        if(fieldName.equals("userName")) {
            return (m_repo.existsByUserName(value.toString()) > 0) ? true : false;            
        }

        throw new UnsupportedOperationException("Field name not supported");
    }    
}
