package com.bedeapps.firstspringproject.api.controller;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bedeapps.firstspringproject.api.model.BookInputModel;
import com.bedeapps.firstspringproject.api.model.BookOutputModel;
import com.bedeapps.firstspringproject.domain.exception.BookNotFoundException;
import com.bedeapps.firstspringproject.domain.model.Book;
import com.bedeapps.firstspringproject.domain.repository.BooksRepository;
import com.bedeapps.firstspringproject.domain.service.BookService;

@RestController
@RequestMapping("/books")
public class BooksController {

    @Autowired
    private BooksRepository m_repository;
    
    @Autowired
    private BookService m_service;
    
    @Autowired
    private ModelMapper m_mapper;
    
    @GetMapping
    public List<BookOutputModel> listar() {		
        return toOutputModelCollection(m_repository.findAll());
    }
    
    @GetMapping("/{bookId}")
    public ResponseEntity<Book> procurar(@PathVariable Long bookId) {
        Optional<Book> book = m_repository.findById(bookId);
        
        if(book.isPresent())
            return ResponseEntity.of(book);
        
        return ResponseEntity.notFound().build();
    }
    
    @GetMapping("title/{bookTitle}")
    public List<Book> procurar(@PathVariable String bookTitle) {
        List<Book> books = m_repository.findByTitle(bookTitle);
        
        if(books.isEmpty())
            throw new BookNotFoundException();
            
        return books;
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookInputModel inserir(@Valid @RequestBody BookInputModel book, Locale locale) {
        Book input = toEntity(book);

        return toInputModel(m_service.save(input));
    }
    
    private Book toEntity(BookInputModel model) {
        return m_mapper.map(model, Book.class);
    }

    private BookInputModel toInputModel(Book book) {
        return  m_mapper.map(book, BookInputModel.class);
    }

    private BookOutputModel toOutputModel(Book book) {
        return  m_mapper.map(book, BookOutputModel.class);
    }

    private List<BookOutputModel> toOutputModelCollection(List<Book> books) {
        return books.stream()
                .map(book -> toOutputModel(book))
                .collect(Collectors.toList());
    }
}
