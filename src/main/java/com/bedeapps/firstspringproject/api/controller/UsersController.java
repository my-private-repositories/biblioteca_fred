package com.bedeapps.firstspringproject.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bedeapps.firstspringproject.api.model.UserModel;
import com.bedeapps.firstspringproject.domain.model.User;
import com.bedeapps.firstspringproject.domain.repository.UsersRepository;
import com.bedeapps.firstspringproject.domain.service.UsersService;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UsersRepository m_repo;

    @Autowired
    private UsersService m_service;

    @Autowired
    private ModelMapper m_mapper;

    @GetMapping
    public List<UserModel> listar() {
        return toModelCollection(m_repo.findAll());
    }

    @PostMapping	
    @ResponseStatus(HttpStatus.CREATED)
    public UserModel inserir(@Valid @RequestBody UserModel model) {	
        UserModel newUser = toModel(m_service.inserir(fromModel(model))); 

        doAutoLogin(model);
        
        return newUser;
    }
    
    private User fromModel(UserModel model) {
        return  m_mapper.map(model, User.class);
    }

    private UserModel toModel(User user) {
        return m_mapper.map(user, UserModel.class);
    }

    private List<UserModel> toModelCollection(List<User> users) {
        return users.stream()
                .map(user -> toModel(user))
                .collect(Collectors.toList());
    }
    
    private void doAutoLogin(UserModel user) {
        String userName = user.getCredential().getUserName();
        String password = user.getCredential().getPassPhrase();
        String role = user.getCredential().getUserRole().toString();

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(role));
        Authentication auth = new UsernamePasswordAuthenticationToken(userName, password, authorities);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }
}
