package com.bedeapps.firstspringproject.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.bedeapps.firstspringproject.api.model.BookGenreModel;
import com.bedeapps.firstspringproject.domain.model.BookGenre;
import com.bedeapps.firstspringproject.domain.repository.BookGenresRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/genres")
public class BookGenresController {
    
    @Autowired
    private BookGenresRepository m_repo;

    @Autowired
    private ModelMapper m_mapper;

    @GetMapping
    public List<BookGenreModel> list() {
        return toModelCollection(m_repo.findAllByOrderByNameAsc());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookGenreModel insert(@Valid @RequestBody BookGenreModel genre) {
        BookGenre input = toEntity(genre);

        return toModel(m_repo.save(input));
    }

    private BookGenre toEntity(BookGenreModel model) {
        return m_mapper.map(model, BookGenre.class);
    }

    private List<BookGenreModel> toModelCollection(List<BookGenre> genres) {
        return genres.stream()
                .map(genre -> toModel(genre))
                .collect(Collectors.toList());
    }

    private BookGenreModel toModel(BookGenre genre) {
        return m_mapper.map(genre, BookGenreModel.class);
    }
}
