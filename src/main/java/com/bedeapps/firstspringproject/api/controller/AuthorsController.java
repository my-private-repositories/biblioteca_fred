package com.bedeapps.firstspringproject.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.bedeapps.firstspringproject.api.model.AuthorModel;
import com.bedeapps.firstspringproject.domain.model.Author;
import com.bedeapps.firstspringproject.domain.repository.AuthorsRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authors")
public class AuthorsController {

    @Autowired
    private AuthorsRepository m_repo;

    @Autowired
    private ModelMapper m_mapper;

    @PostMapping
    public AuthorModel insert(@Valid @RequestBody AuthorModel author) {
        Author input = toEntity(author);
        return toModel(m_repo.save(input));
    }

    @GetMapping
    public List<AuthorModel> list() {
        return toModelCollection(m_repo.findAllByOrderByFirstNameAsc());
    }

    private Author toEntity(AuthorModel model) {
        return m_mapper.map(model, Author.class);
    }

    private List<AuthorModel> toModelCollection(List<Author> authors) {
        return authors.stream()
                .map(author -> toModel(author))
                .collect(Collectors.toList());
    }

    private AuthorModel toModel(Author author) {
        return m_mapper.map(author, AuthorModel.class);
    }
}
