package com.bedeapps.firstspringproject.api.exceptionhandler;

import java.time.OffsetDateTime;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bedeapps.firstspringproject.api.model.BookInputModel;
import com.bedeapps.firstspringproject.core.Constants;
import com.bedeapps.firstspringproject.domain.exception.BookNotFoundException;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private final MessageSource m_msgSrc;

    @Autowired
    public ApiExceptionHandler() { 
        m_msgSrc = Constants.messageSource();
    }
    
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
                
        var campos = new ArrayList<Problema.Campo>();

        for(ObjectError er: ex.getAllErrors()) {
            String nome = BookInputModel.mapFieldToEntity(((FieldError)er).getField());
            String msg = m_msgSrc.getMessage(er.getDefaultMessage(), er.getArguments(), request.getLocale());
            
            campos.add(new Problema.Campo(nome, msg));
        }
        
        var prob = new Problema();
        prob.setStatus(status.value());
        prob.setTitulo("Um ou mais campos estão inválidos."
                + "Preencha corretamente e tente novamente.");

        prob.setDataHora(OffsetDateTime.now());
        prob.setCampos(campos);		
        
        return super.handleExceptionInternal(ex, prob, headers, status, request);
    }
    
    @ExceptionHandler({BookNotFoundException.class})
    private ResponseEntity<Object> handleNotFound(Exception ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), 
                HttpStatus.NOT_FOUND, request);
    }
}
