package com.bedeapps.firstspringproject.api.exceptionhandler;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Problema {
    
    private int m_status;
    private String m_titulo;
    private OffsetDateTime m_dataHora;
    private List<Campo> m_campos;
    
    public int getStatus() {
        return m_status;
    }

    public void setStatus(int status) {
        this.m_status = status;
    }

    public String getTitulo() {
        return m_titulo;
    }

    public void setTitulo(String titulo) {
        this.m_titulo = titulo;
    }

    public OffsetDateTime getDataHora() {
        return m_dataHora;
    }

    public void setDataHora(OffsetDateTime dataHora) {
        this.m_dataHora = dataHora;
    }

    public List<Campo> getCampos() {
        return m_campos;
    }

    public void setCampos(List<Campo> campos) {
        this.m_campos = campos;
    }
    
    public static class Campo {
        private String m_campo;
        private String m_msg;
        
        public Campo(String campo, String msg) {
            super();
            
            m_campo = campo;
            m_msg = msg;
        }

        public String getCampo() {
            return m_campo;
        }

        public void setCampo(String campo) {
            this.m_campo = campo;
        }

        public String getMsg() {
            return m_msg;
        }

        public void setMsg(String msg) {
            this.m_msg = msg;
        }		
    }
}
