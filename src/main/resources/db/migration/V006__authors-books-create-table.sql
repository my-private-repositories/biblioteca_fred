CREATE TABLE books_authors (
  book_id bigint NOT NULL,
  author_id bigint NOT NULL,
  PRIMARY KEY (book_id, author_id),
  CONSTRAINT fk_book_books_authors 
	FOREIGN KEY (book_id) 
	  REFERENCES books (id)
		ON DELETE CASCADE,
  CONSTRAINT fk_authors_books_authors 
	FOREIGN KEY (author_id) 
	  REFERENCES authors (id)
		ON DELETE CASCADE
); 