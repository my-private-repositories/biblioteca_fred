CREATE TABLE genres (
  id bigint NOT NULL AUTO_INCREMENT,
  name varchar(60) NOT NULL,
  description varchar(256) NOT NULL,
  PRIMARY KEY (id)
) 