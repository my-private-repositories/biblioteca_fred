CREATE TABLE users_credential (
	user_id BIGINT NOT NULL,
	user_name VARCHAR(60) NOT NULL,
	pass_phrase text NOT NULL,
	user_role VARCHAR(10) NOT NULL,
		
	PRIMARY KEY (user_id),
	UNIQUE (user_name),
	CONSTRAINT fk_user_credential_user 
		FOREIGN KEY (user_id) 
			REFERENCES users (id)
			ON DELETE CASCADE
);