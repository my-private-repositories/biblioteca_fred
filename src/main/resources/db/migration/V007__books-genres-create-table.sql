CREATE TABLE books_genres (
  book_id bigint NOT NULL,
  genre_id bigint NOT NULL,
  PRIMARY KEY (book_id, genre_id),
  CONSTRAINT fk_book_books 
	FOREIGN KEY (book_id) 
	  REFERENCES books (id)
		ON DELETE CASCADE,
  CONSTRAINT fk_books_genres 
	FOREIGN KEY (genre_id) 
	  REFERENCES genres (id)
		ON DELETE CASCADE
); 