CREATE TABLE authors (
  id bigint NOT NULL AUTO_INCREMENT,
  first_name varchar(60) NOT NULL,
  last_name varchar(60) NOT NULL,
  description varchar(256) NOT NULL,
  PRIMARY KEY (id),

  INDEX (first_name, last_name)
) 