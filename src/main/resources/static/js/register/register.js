function checkErrors(json) {

    var errors = {
        firstName: "",
        lastName: "",
        address: "",
        email: "",
        userName: "",
        passPhrase: ""
    }

    if (json !== undefined) {
        const objFields = json.campos;

        for (const fieldError of Object.entries(objFields)) {
            const field = fieldError[1].campo;
            const msg = fieldError[1].msg;

            var idx = field.indexOf("userName");

            if (idx >= 0) {
                errors["userName"] = msg;
            } else {
                idx = field.indexOf("passPhrase");

                if (idx >= 0) {
                    errors["passPhrase"] = msg;
                } else {
                    errors[field] = msg;
                }
            }
        }
    }

    showOrHideError(errors);
}

function showOrHideError(errors) {
    for (const [key, value] of Object.entries(errors)) {
        addOrRemoveErrorMessage(key, value);
    }
}

function addOrRemoveErrorMessage(field, msg) {
    var componentId = '#' + field + 'Div';
    var labelId = field + '_error';

    if (msg) {
        var label = '<p class="text-danger" id="' + labelId +
            '" align="center">' + msg + '</p>';

        var element = document.getElementById(labelId);
        if (element !== null) {
            element.innerHTML = msg;
        } else {
            $(`${componentId}`).append(label);
        }
    } else {
        var element = document.getElementById(labelId);

        if (element !== null) {
            var errorLabel = '#' + labelId;
            $(`${errorLabel}`).remove();
        }
    }
}

function submitData(json) {
    var json = $('#mainForm').serializeJSON();

    json.credential.userRole = "USER";

    $.ajax({
        type: 'post',
        url: '/users',
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function (data) {
            window.location.replace("/");
        },
        error: function (data) {
            var json = JSON.parse(JSON.stringify(data));
            checkErrors(json.responseJSON);
        }
    });

    return false;
}
