function update(input) {
    return parseFloat(input.value.replace(/(.*){1}/, '0$1')
        .replace(/[^\d]/g, '')
        .replace(/(\d\d?)$/, '.$1'))
        .toFixed(2);
}

$(document).ready(function () {

    // Handling price formatting
    var priceInput = document.getElementById('price');

    priceInput.addEventListener('keyup', function () {
        this.value = update(this);
    });

    priceInput.addEventListener('click', function () {
        this.value = parseFloat(this.value).toFixed(2);
    });
    // ------------------------------------------------

    // Handling authors buttons
    var author = document.getElementById('author0');
    addEventHandling(author);
    addButtonsHandlers(0);
    updateButtonsVisibility(0, 0);
    // ------------------------------------------------

    // Handling genre buttons
    var genre = document.getElementById('genre0');
    addEventHandling(genre);
    addButtonsHandlers(1);
    updateButtonsVisibility(0, 1);
    // ------------------------------------------------

    // getSelectValues(0);
    // getSelectValues(1);
});
