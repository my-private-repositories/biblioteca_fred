var typesMDesc = ['Author', 'Genre'];
var typesDesc = ['author', 'genre'];
var typeName = ['Autor', 'Gênero'];

function addSelectValues(idx, type) {
    var selectId = '#' + typesDesc[type] + idx;
    var selectTypeId = typesDesc[type] + '0';
    var select = document.getElementById(selectTypeId);
    var selectOptions = Array.apply(null, select.options);

    var options = '';
    for(selectOption of selectOptions)
        if(selectOption.value != -1 && selectOption.value != -2) 
            options += '<option value="' + selectOption.value + '">' + selectOption.text + '</option>'

    $(selectId).append(options);
}

function updateButtonsVisibility(idx, type) {
    var selCount = getSelectCount(type);
    var addBtn = getButton('add', type);
    var remBtn = getButton('remove', type);

    console.log('count: ' + selCount);
    console.log('idx: ' + idx);
    console.log('type: ' + type);

    // Update visibility
    remBtn.style.display = (idx == 0) ? 'none' : 'inline';
    addBtn.style.display = (idx > (selCount - 2)) ? 'none' : 'inline';
}

function getNewElement(idx, type) {
    var div =  typesDesc[type] + 'Div' + idx;
    var id = typesDesc[type] + idx;
    var btnId = 'buttons' + typesMDesc[type] + 'Group' + idx;

    var newDiv = 
    '<div id="' + div + '">' +
        '<div class="input-group mb-3" id="' + btnId + '">' +
            '<div class="input-group-prepend"> ' +
                '<span class="input-group-text">' + typeName[type] + ':</span>' +
            '</div>' +
            '<select id="' + id + '" name="' + typesDesc[type] + '"></select>' +
        '</div>' +
    '</div>';

    return newDiv;
}

function getSelectCount(type) {
    var divElementId = typesDesc[type] + '0';
    return document.getElementById(divElementId).length -2;
}

function removeDiv(type) {
    var cardId = 'card' + typesMDesc[type];
    var cardCount = document.getElementById(cardId).children.length;
    var divIdx = cardCount - 1;
    var divName = '#' + typesDesc[type] + 'Div' + divIdx;

    // Insert buttons in previous div element
    appendDivButtons(divIdx - 1, type);

    $(divName).remove();
}

function newElement(type) {
    var cardId = 'card' + typesMDesc[type]
    var cardCount = document.getElementById(cardId).children.length;
    var cardName = '#' + cardId;

    var newDiv = getNewElement(cardCount, type);
    $(cardName).append(newDiv);

    addSelectValues(cardCount, type);
    appendDivButtons(cardCount, type);
}

function appendDivButtons(idx, type) {
    var buttonsDiv = '#buttons' + typesMDesc[type] + 'Group' + idx;
    var btnGroupId = 'btns' + typesMDesc[type];
    var buttons = document.getElementById(btnGroupId);

    $(buttonsDiv).append(buttons);

    updateButtonsVisibility(idx, type);
}

function getButton(name, type) {
    var addBtnId = name + typesMDesc[type]; 
    return document.getElementById(addBtnId);
}

function addButtonsHandlers(type) {
    var addBtn = getButton('add', type);
    var remBtn = getButton('remove', type);

    addAddHandling(addBtn, type);
    addRemoveHandling(remBtn, type);
}

function getSelectValues(type) {
    var optElems = document.getElementsByName(typesDesc[type]);

    var elemsIds = [];

    for (optElem of optElems)
        elemsIds[elemsIds.length] = { "id": optElem.value };

    console.log('elems: ' + JSON.stringify(elemsIds));
}

function addEventHandling(select) {
    select.addEventListener('change', function () {
        var option = this.value;

        if (option == -1) {
            console.log('Novo: ' + this.id);
        }
    });
}

function addAddHandling(addButton, type) {
    addButton.addEventListener('click', function () {
        newElement(type);
    })
}

function addRemoveHandling(removeButton, type) {
    removeButton.addEventListener('click', function () {
        removeDiv(type);
    })
}
