FROM adoptopenjdk/openjdk11

LABEL maintainer="bede.apps@gmail.com"

WORKDIR /app

ARG JAR_FILE=target/*.jar

COPY ${JAR_FILE} ./library.jar

ENTRYPOINT ["java","-jar","library.jar"]