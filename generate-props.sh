#!/bin/bash

ADMIN_PASSWORD=$1
KEY=$2
OUT_PROPS_FILE=src/main/resources/props

cat <<EOL > props
spring.datasource.url"="jdbc:mysql://localhost:33061/library?createDatabaseIfNotExist=true&serverTimezone=UTC;spring.datasource.username"="admin;spring.datasource.password"="${ADMIN_PASSWORD}
EOL

openssl enc -aes-256-cbc -salt -a -in props -out $OUT_PROPS_FILE -pass pass:$KEY -md sha1