#!/bin/bash

KEY=$(cat < KEY)

cat <<EOF > docker-compose.yaml
version: '3'

services:

  app:
    build: .
    container_name: library-app
    depends_on:
       - db
    networks:
      - library-network
    ports:
      - 8081:8081
    environment:
      - SPRING_PROFILES_ACTIVE=deploy
      - SECRET=${KEY}
    restart: on-failure

  db:
    build: .docker/mysql
    container_name: mysql-db
    volumes: 
      - .docker/mysql/.init:/docker-entrypoint-initdb.d
      - dbdata:/var/lib/mysql
    ports:
      - 33061:3306
    networks:
      - library-network
    restart: always
    environment:
      - MYSQL_ROOT_PASSWORD=root
    
volumes:
  dbdata:

networks:
  library-network:
    driver: bridge
EOF