#!/bin/bash

./reset-mysql.sh
./reset-app.sh

mvn clean > /dev/null 2>&1

docker-compose down --volumes > /dev/null 2>&1

DIR=$(echo $PWD)
PREFIX=$(echo ${DIR%/*})
IMAGE_NAME=$(echo ${DIR#$PREFIX/})
DB=$(echo "${IMAGE_NAME}_db")
APP=$(echo "${IMAGE_NAME}_app")

docker rmi $DB $APP > /dev/null 2>&1

echo "Docker environment clear!"